let collection = [];
// Write the queue functions below.

function print(){
    return collection
}

function enqueue(item){
    collection.push(item)
    return collection
}

function dequeue(){
    collection.shift()
    return collection
}

function front(){
    return collection[0]
}

function size(){
    return collection.length
}

function isEmpty(){
    let result = collection.length == 0
    return result ? true : false
}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};